using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Assignment6.DTO.CharacterDTOs;
using Assignment6.DTO.MovieDTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Assignment6.Controllers;

[Route("api/movies")]
[ApiController]
[Produces(MediaTypeNames.Application.Json)]
[Consumes(MediaTypeNames.Application.Json)]
[ApiConventionType(typeof(DefaultApiConventions))]
public class MovieController : ControllerBase
{
    private readonly CharacterDbContext _context;
    private readonly IMapper _mapper;

    public MovieController(CharacterDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Gets all movies
    /// </summary>
    /// <returns>A list of movies(ReadMovieDTOs) from the database</returns>
    [HttpGet]
    public async Task<ActionResult<IEnumerable<ReadMovieDTO>>> GetMovies()
    {
        var movies = await _context.Movies
            .Include(m => m.Franchise)
            .Include(m => m.Characters)
            .ToListAsync();
        var readMovies = _mapper.Map<List<ReadMovieDTO>>(movies);
        
        return readMovies;
    }

    /// <summary>
    /// Gets a specific movie by ID
    /// </summary>
    /// <param name="id">The ID of requested movie</param>
    /// <returns>One movie(ReadMovieDTO) from the database</returns>
    [HttpGet("{id}")]
    public async Task<ActionResult<ReadMovieDTO>> GetMovieById(int id)
    {
        var movie = await _context.Movies
            .Include(m => m.Franchise)
            .Include(m => m.Characters)
            .Where(m => m.Id == id)
            .FirstOrDefaultAsync();
        
        if (movie == null)
          return NotFound("There's no movie with this id");

        var readMovieDto = _mapper.Map<ReadMovieDTO>(movie);
        
        return readMovieDto;
    }


    /// <summary>
    /// Adds a movie 
    /// </summary>
    /// <param name="movieDto">All the fields(ReadMovieDTO) that need to be added</param>
    /// <returns>The franchise(ReadMovieDTO) that is successfully added to the database</returns>
    [HttpPost]
    public async Task<ActionResult<ReadMovieDTO>> AddMovie([FromBody] CreateMovieDTO movieDto)
    {
        var domainMovie = _mapper.Map<Movie>(movieDto);

        try
        {
            _context.Movies.Add(domainMovie);
            await _context.SaveChangesAsync();
        }
        catch
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        var newMovie = _mapper.Map<ReadMovieDTO>(domainMovie);
        
        return CreatedAtAction("GetMovieById", new { Id = newMovie.Id}, newMovie);
    }

    /// <summary>
    /// Updates a movie found by ID
    /// </summary>
    /// <param name="id">The ID of the movie that needs to be updated</param>
    /// <param name="updatedMovie">All the fields(UpdateMovieDTO) that need to be updated</param>
    /// <returns>The updated movie when successfully updated</returns>
    [HttpPut]
    public async Task<ActionResult> UpdateMovie(int id, [FromBody] UpdateMovieDTO updatedMovie)
    {
        if (id != updatedMovie.Id)
        {
            return NotFound("The id's don't match");
        }

        if (!MovieExists(id))
        {
            return NotFound("There's no movie with this id");
        }

        try
        {
            var domainMovie = _mapper.Map<Movie>(updatedMovie);
            _context.Entry(domainMovie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        catch
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        return Ok(updatedMovie);
    }

    /// <summary>
    /// Deletes a franchise found by ID
    /// </summary>
    /// <param name="deleteMovie">The ID(DeleteMovieDTO) of the character that needs to be deleted</param>
    /// <returns>Saves changes to database, but doesn't return anything</returns>
    [HttpDelete]
    public async Task<ActionResult> DeleteMovie(DeleteMovieDTO deleteMovie)
    {
        var movie = await _context.Movies.FindAsync(deleteMovie.Id);
        if (movie == null)
            return NotFound("There's no movie with this id");

        try
        {
            var domainMovie = _mapper.Map<Movie>(movie);
            _context.Movies.Remove(domainMovie);
            await _context.SaveChangesAsync();
        }
        catch
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        return NoContent();
    }

    /// <summary>
    /// Helper method to check whether the movie exists in the database
    /// </summary>
    /// <param name="id">The Id of the movie to be checked</param>
    /// <returns>True if movie exists in database, but false if it doesn't</returns>
    private bool MovieExists(int id)
    {
        return _context.Movies.Any(e => e.Id == id);
    }


    /// <summary>
    /// Updates the list of characterIDs from a movie 
    /// </summary>
    /// <param name="id">The ID of the movie where the characterIDs need to be added to</param>
    /// <param name="characterIds">A list of integers that represent the characterIDs that need to be added to the movie</param>
    /// <returns>Saves changes to database, but doesn't return anything</returns>
    [HttpPut("{id}/Character")]
    public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characterIds)
    {
        if (!MovieExists(id))
        {
            return NotFound("This movie id doesn't exist");
        }

        Movie movieToUpdateCharacters = await _context.Movies
            .Include(c => c.Characters)
            .Where(c => c.Id == id)
            .FirstAsync();
        
        // Set the characters that already exist in the franchise to a new list
        var characters = movieToUpdateCharacters.Characters;
        // Loop through the character list from parameter, try and assign it to the movie
        foreach (int characterId in characterIds)
        {
            Character character = await _context.Character.FindAsync(characterId);
            if (character == null)
                return BadRequest($"Character with the id of {characterId} doesn't exist!");
            characters.Add(character);
        }

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            throw;
        }

        return NoContent();
    }
    
    /// <summary>
    /// Get a list of all characters in a movie
    /// </summary>
    /// <param name="id">The ID of the movie to get the characters from</param>
    /// <returns>A list of characters(ReadCharacterDTO) from the database</returns>
    [HttpGet("{id}/Characters")]
    public async Task<ActionResult<List<ReadCharacterDTO>>> GetCharactersInMovie(int id)
    {
        if (!MovieExists(id))
        {
            return NotFound("This movie id doesn't exist");
        }
        
        var movie = await _context.Movies
            .Include(m => m.Characters)
            .Where(m => m.Id == id)
            .FirstOrDefaultAsync();

        var charactersInMovie = movie.Characters;
        if (charactersInMovie.Count == 0)
            return NotFound("No characters found in this movie");

        var characters = new List<ReadCharacterDTO>();
        foreach (var character in charactersInMovie)
        {
            characters.Add(_mapper.Map<ReadCharacterDTO>(character));
        }
        
        return Ok(characters);
    }
}
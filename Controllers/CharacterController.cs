﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Assignment6.DTO.CharacterDTOs;
using Assignment6.DTO.MovieDTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Assignment6.Controllers
{
    [Route("api/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharacterController : ControllerBase
    {
        private readonly CharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharacterController(CharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all characters
        /// </summary>
        /// <returns>A list of characters(ReadCharacterDTOs) from the database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadCharacterDTO>>> GetCharacters()
        {
            var characters = await _context.Character.Include(c => c.Movies).ToListAsync();
            var readCharacters = _mapper.Map<List<ReadCharacterDTO>>(characters);

            return readCharacters;
        }

        /// <summary>
        /// Gets a specific character by ID
        /// </summary>
        /// <param name="id">The ID of requested character</param>
        /// <returns>One character(ReadCharacterDTO) from the database</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadCharacterDTO>> GetCharacterById(int id)
        {
            var character = await _context.Character.Include(c => c.Movies)
                .Where(c => c.Id == id)
                .FirstOrDefaultAsync();
            if (character == null)
              return NotFound("There's no character with this id");

            var readCharacterDto = _mapper.Map<ReadCharacterDTO>(character);

            return readCharacterDto;
        }

        /// <summary>
        /// Adds one character
        /// </summary>
        /// <param name="characterDto">All the fields(CreateCharacterDTO) that need to be added</param>
        /// <returns>The character(ReadCharacterDTO) that is successfully added to the database</returns>
        [HttpPost]
        public async Task<ActionResult<ReadCharacterDTO>> AddCharacter([FromBody] CreateCharachterDTO characterDto)
        {
            var domainCharacter = _mapper.Map<Character>(characterDto);

            try
            {
                _context.Character.Add(domainCharacter);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            var newCharacter = _mapper.Map<ReadCharacterDTO>(domainCharacter);

            return CreatedAtAction("GetCharacterById", new { Id = newCharacter.Id}, newCharacter);
        }

        /// <summary>
        /// Updates a character found by ID
        /// </summary>
        /// <param name="id">The ID of the character that needs to be updated</param>
        /// <param name="updatedCharacter">All the fields that need to be updated defined by UpdateCharacterDTO</param>
        /// <returns>The updated character when successfully updated</returns>
        [HttpPut]
        public async Task<ActionResult> UpdateCharacter(int id, [FromBody] UpdateCharacterDTO updatedCharacter)
        {
            if (id != updatedCharacter.Id)
            {
                return NotFound("The id's don't match");
            }
            
            if (!CharacterExists(id))
            {
                return NotFound("This movie id doesn't exist");
            }

            try
            {
                var domainCharacter = _mapper.Map<Character>(updatedCharacter);
                _context.Entry(domainCharacter).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok(updatedCharacter);
        }

        /// <summary>
        /// Deletes a character found by ID
        /// </summary>
        /// <param name="deleteCharacter">The ID (DeleteCharacterDTO) of the character that needs to be deleted</param>
        /// <returns>Saves changes to database, but doesn't return anything</returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteCharacter(DeleteCharacterDTO deleteCharacter)
        {
            var character = await _context.Character.FindAsync(deleteCharacter.Id);
            if (character == null)
                return NotFound("There's no character with this id");

            try
            {
                var domainCharacter = _mapper.Map<Character>(character);
                _context.Character.Remove(domainCharacter);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return NoContent();
        }
        
        /// <summary>
        /// Helper method to check whether the movie exists in the database
        /// </summary>
        /// <param name="id">The Id of the movie to be checked</param>
        /// <returns>True if movie exists in database, but false if it doesn't</returns>
        private bool CharacterExists(int id)
        {
            return _context.Character.Any(c => c.Id == id);
        }

    }
}

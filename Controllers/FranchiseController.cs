using System.Net.Mime;
using Assignment6.DTO.CharacterDTOs;
using Assignment6.DTO.FranchiseDTOs;
using Assignment6.DTO.MovieDTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Assignment6.Controllers;

[Route("api/franchises")]
[ApiController]
[Produces(MediaTypeNames.Application.Json)]
[Consumes(MediaTypeNames.Application.Json)]
[ApiConventionType(typeof(DefaultApiConventions))]
public class FranchiseController : ControllerBase
{
    private readonly CharacterDbContext _context;
    private readonly IMapper _mapper;

    public FranchiseController(CharacterDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }
    
    /// <summary>
    /// Gets all franchises
    /// </summary>
    /// <returns>A list of franchises(ReadFranchiseDTOs) from the database</returns>
    [HttpGet]
    public async Task<ActionResult<IEnumerable<ReadFranchiseDTO>>> GetFranchise()
    {
        var franchises = await _context.Franchises.Include(f => f.Movies).ToListAsync();
        var readFranchises = _mapper.Map<List<ReadFranchiseDTO>>(franchises);

        return readFranchises;
    }

    /// <summary>
    /// Gets a specific franchise by ID
    /// </summary>
    /// <param name="id">The ID of requested franchise</param>
    /// <returns>One franchise(ReadFranchiseDTO) from the database</returns>
    [HttpGet("{id}")]
    public async Task<ActionResult<ReadFranchiseDTO>> GetFranchiseById(int id)
    {
        var franchise = await _context.Franchises.Include(f => f.Movies)
            .Where(f => f.Id == id)
            .FirstOrDefaultAsync();
        if (franchise == null)
            return NotFound("There's no franchise with this id");

        var readFranchiseDto = _mapper.Map<ReadFranchiseDTO>(franchise);

        return readFranchiseDto;
    }

    /// <summary>
    /// Adds one franchise
    /// </summary>
    /// <param name="franchiseDto">All the fields(CreateFranchiseDTO) that need to be added</param>
    /// <returns>The franchise(ReadFranchiseDTO) that is successfully added to the database</returns>
    [HttpPost]
    public async Task<ActionResult<ReadFranchiseDTO>> AddFranchise([FromBody] CreateFranchiseDTO franchiseDto)
    {
        var domainFranchise = _mapper.Map<Franchise>(franchiseDto);

        try
        {
            _context.Franchises.Add(domainFranchise);
            await _context.SaveChangesAsync();
        }
        catch
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        var newFranchise = _mapper.Map<ReadFranchiseDTO>(domainFranchise);

        return CreatedAtAction("GetFranchiseByID", new { Id = newFranchise}, newFranchise);
    }

    /// <summary>
    /// Updates a franchise found by ID
    /// </summary>
    /// <param name="id">The ID of the franchise that needs to be updated</param>
    /// <param name="updatedFranchise">All the fields(UpdateFranchiseDTO) that need to be updated</param>
    /// <returns>The updated franchise when successfully updated</returns>
    [HttpPut]
    public async Task<ActionResult> UpdateFranchise(int id, [FromBody] UpdateFranchiseDTO updatedFranchise)
    {
        if (id != updatedFranchise.Id)
        {
            return NotFound("The id's don't match");
        }
        
        if (!FranchiseExists(id))
        {
            return NotFound("There's no franchise with this id");
        }

        try
        {
            var domainFranchise = _mapper.Map<Franchise>(updatedFranchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        catch
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        return Ok(updatedFranchise);
    }

    /// <summary>
    /// Deletes a franchise found by ID
    /// </summary>
    /// <param name="deleteFranchise">The ID(DeleteFranchiseDTO) of the character that needs to be deleted</param>
    /// <returns>Saves changes to database, but doesn't return anything</returns>
    [HttpDelete]
    public async Task<ActionResult> DeleteFranchise(DeleteFranchiseDTO deleteFranchise)
    {
        var franchise = await _context.Franchises.FindAsync(deleteFranchise.Id);
        var moviesInFranchise = await _context.Movies
            .Where(m => m.FranchiseId == deleteFranchise.Id)
            .ToListAsync();

        if (franchise == null)
            return NotFound("There's no franchise with this id");

        try
        {
            // Loops through every movie in the list of movieIds in the franchise
            foreach (var movie in moviesInFranchise)
            {
                // Sets franchiseId to null before deleting the franchise
                movie.FranchiseId = null;
            }
            
            var domainFranchise = _mapper.Map<Franchise>(franchise);
            _context.Franchises.Remove(domainFranchise);
            await _context.SaveChangesAsync();
        }
        catch
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        return NoContent();
    }
    
    /// <summary>
    /// Helper method to check whether the franchise exists in the database
    /// </summary>
    /// <param name="id">The Id of the franchise to be checked</param>
    /// <returns>True if franchise exists in database, but false if it doesn't</returns>
    private bool FranchiseExists(int id)
    {
        return _context.Franchises.Any(e => e.Id == id);
    }
    
    /// <summary>
    /// Updates the list of movieIDs from a franchise
    /// </summary>
    /// <param name="id">The ID of the franchise where the movieIDs need to be added to</param>
    /// <param name="movieIds">A list of integers that represent the movie IDs that need to be added to the franchise</param>
    /// <returns>Saves changes to database, but doesn't return anything</returns>
    [HttpPut("{id}/Movie")]
    public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movieIds)
    {
        if (!FranchiseExists(id))
        {
            return NotFound("This franchise id doesn't exist");
        }
        
        Franchise franchiseToUpdateMovies = await _context.Franchises
            .Include(f => f.Movies)
            .Where(f => f.Id == id)
            .FirstAsync();

        // Set the movies that already exist in the franchise to a new list
        var movies = franchiseToUpdateMovies.Movies;
        // Loop through the movie list from parameter, try and assign it to the franchise
        foreach (int movieId in movieIds)
        {
            Movie movie = await _context.Movies.FindAsync(movieId);
            if (movie == null)
                return BadRequest($"Movie with the id of {movieId} doesn't exist!");
            movies.Add(movie);
        }

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            throw;
        }

        return NoContent();
    }
    
    /// <summary>
    /// Gets a list of all movies from a franchise
    /// </summary>
    /// <param name="id">The ID of the franchise to get the movies from</param>
    /// <returns>A list of movies(ReadMovieDTO) from the database</returns>
    [HttpGet("{id}/Movies")]
    public async Task<ActionResult<List<ReadMovieDTO>>> GetMoviesInFranchise(int id)
    {
        if (!FranchiseExists(id))
        {
            return NotFound("There's no franchise with this id");
        }
        
        var movies = await _context.Movies
            .Include(m => m.Franchise)
            .Include(m => m.Characters)
            .Where(m => m.FranchiseId == id)
            .ToListAsync();

        if (movies.Count == 0)
            return NotFound("No movies found in this franchise");

        var readMovieDto = new List<ReadMovieDTO>();
        foreach (var movie in movies)
        {
            readMovieDto.Add(_mapper.Map<ReadMovieDTO>(movie));
        }

        return Ok(readMovieDto);
    }
    
    /// <summary>
    /// Gets a list of all characters from a franchise
    /// </summary>
    /// <param name="id">The ID of the franchise to get the characters from</param>
    /// <returns>A list of characters(ReadCharacterDTO) from the database</returns>
    [HttpGet("{id}/Characters")]
    public async Task<ActionResult<List<ReadCharacterDTO>>> GetCharactersInFranchise(int id)
    {
        if (!FranchiseExists(id))
        {
            return NotFound("There's no franchise with this id");
        }
        
        // Gets a list of movies from specific franchise
        var movies = await _context.Movies
            .Include(m => m.Characters)
            .Where(m => m.FranchiseId == id)
            .ToListAsync();
    
        if (movies.Count() == 0)
            return NotFound("No movies found in this franchise");

        // Adds all characters to a list that belong to a certain movie from the requested franchise
        var characterList = new List<ReadCharacterDTO>();
        // Loops through every movie in list of movies
        foreach (var movie in movies)
        {
            // Loops through every character in the movies characters
            foreach (var character in movie.Characters)
            {
                characterList.Add(_mapper.Map<ReadCharacterDTO>(character));
            }
        }

        if (characterList.Count() == 0)
            return NotFound("No characters found in this franchise");
        
        return Ok(characterList.GroupBy(p => p.Id).Select(grp => grp.First()));
    }
}
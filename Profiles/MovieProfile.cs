using Assignment6.DTO.MovieDTOs;
using AutoMapper;

namespace Assignment6.Profiles;

public class MovieProfile : Profile
{
    public MovieProfile()
    {
        CreateMap<Movie, ReadMovieDTO>()
            .ForMember(rmdto => rmdto.FranchiseId,
            opt => opt
                .MapFrom(m => m.Franchise.Id))
            .ForMember(rmdto => rmdto.CharacterIds,
                opt => opt
                .MapFrom(m => m.Characters
                    .Select(c => c.Id)
                    .ToList()));
        CreateMap<CreateMovieDTO, Movie>();
        CreateMap<UpdateMovieDTO, Movie>();
        CreateMap<DeleteMovieDTO, Movie>();
    }
    
}
using Assignment6.DTO.FranchiseDTOs;
using AutoMapper;

namespace Assignment6.Profiles;

public class FranchiseProfile : Profile
{
    public FranchiseProfile()
    {
        CreateMap<Franchise, ReadFranchiseDTO>().ForMember(rfdto => rfdto.MovieIds, 
            opt => opt.MapFrom(f => f.Movies
                .Select(m => m.Id)
                .ToArray()));
        CreateMap<CreateFranchiseDTO, Franchise>();
        CreateMap<UpdateFranchiseDTO, Franchise>();
        CreateMap<DeleteFranchiseDTO, Franchise>();
    }
}
using Assignment6.DTO.CharacterDTOs;
using AutoMapper;

namespace Assignment6.Profiles;

public class CharacterProfile : Profile
{
    public CharacterProfile()
    {
        CreateMap<Character, ReadCharacterDTO>()
            .ForMember(rcdto => rcdto.MovieIds, 
            opt => opt.MapFrom(c => c.Movies
                .Select(m=>m.Id)
                .ToList()));
        CreateMap<CreateCharachterDTO, Character>();
        CreateMap<UpdateCharacterDTO, Character>();
        CreateMap<DeleteCharacterDTO, Character>();
    }
}
﻿using System;
using System.Collections.Generic;
using Assignment6.Models;
using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;



namespace Assignment6.Data
{
    public class CharacterDbContext : DbContext
    {
        public CharacterDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Character> Character { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data for character
            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, FullName = "Jonny Deep", Alias = "Jack Sparrow", Gender = "Male", Picture = "https://www.pinterest.com/pin/907123549913878412/" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 2, FullName = "Angelina Jolie", Alias = "Mrs.Smith", Gender = "Female", Picture = "https://www.pinterest.com/pin/760193612090922976/" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 3, FullName = "Leonardo DiCaprio", Alias = "Mr.GastBy", Gender = "Male", Picture = "https://www.pinterest.com/pin/292100725803211271/" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 4, FullName = "Ryan Reynolds", Alias = "Deadpool", Gender = "Male", Picture = "https://en.wikipedia.org/wiki/Ryan_Reynolds#/media/File:Deadpool_2_Japan_Premiere_Red_Carpet_Ryan_Reynolds_(cropped).jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 5, FullName = "Will Smith", Alias = "Men in Black", Gender = "Male", Picture = "https://en.wikipedia.org/wiki/Will_Smith#/media/File:TechCrunch_Disrupt_2019_(48834434641)_(cropped).jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 6, FullName = "Keanu Reeves", Alias = "Jonh Wick", Gender = "Male", Picture = "https://en.wikipedia.org/wiki/Keanu_Reeves#/media/File:Reuni%C3%A3o_com_o_ator_norte-americano_Keanu_Reeves_cropped_2_(46806576944)_(cropped).jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 7, FullName = "Robert Downey Jr.", Alias = "Jonny Stark", Gender = "Male", Picture = "https://en.wikipedia.org/wiki/Robert_Downey_Jr.#/media/File:Robert_Downey_Jr_2014_Comic_Con_(cropped).jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 8, FullName = "Charlize Theron", Alias = "", Gender = "Female", Picture = "https://en.wikipedia.org/wiki/Charlize_Theron#/media/File:Charlize-theron-IMG_6045.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 9, FullName = "Scarlett Johansson", Alias = "Black widow", Gender = "Female", Picture = "https://en.wikipedia.org/wiki/Scarlett_Johansson#/media/File:Scarlett_Johansson_by_Gage_Skidmore_2_(cropped,_2).jpg" });
            
            // Seed data for Franchise
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 1, Name = "Marvel Cinematic Universe", Description = "With 23 movies released in just 12 years, the Marvel Cinematic Universe is one of the most prolific franchises in cinema history. And there are more movies on the way." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 2, Name = "DC Extended Universe", Description = "Launching in 2013 with 'Man of Steel' the DC Extended Universe was Warner Bros. response to the success of Disney’s series of Marvel films." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 3, Name = "X-Men", Description = "Starting in 2000, 13 films set within the X-Men Universe were released by 20th Century Fox. In total, these films collected more than $6 billion in ticket sales globally." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 4, Name = "Tranformers", Description = "Transformers is a series of American science fiction action films based on the Transformers franchise, which began in the 1980s.[" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 5, Name = "Pirates of the Caribbean", Description = "The film series serves as a major component of the eponymous media franchise." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 6, Name = "The Matrix", Description = "The Matrix is an American media franchise consisting of four feature films, beginning with The Matrix (1999) and continuing with three sequels." });
            
            // Seeding movies
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, Title = "Iron Man", FranchiseId = 1, Genre = "Action, Sci-Fi", ReleaseYear = 2008, Director = "Jon Favreau", PictureUrl = "https://en.wikipedia.org/wiki/Iron_Man_(2008_film)#/media/File:Iron_Man_(2008_film)_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=erbA88h1s4o" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 2, Title = "The Incredible Hulk", FranchiseId = 1, Genre = "Action, Sci-Fi", ReleaseYear = 2008, Director = "Louis Leterrier", PictureUrl = "https://en.wikipedia.org/wiki/The_Incredible_Hulk_(film)#/media/File:The_Incredible_Hulk_(film)_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=xbqNb2PFKKA" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, Title = "Deadpool", FranchiseId = 3, Genre = "Action, Sci-Fi, Comedy", ReleaseYear = 2016, Director = "Tim Miller", PictureUrl = "https://en.wikipedia.org/wiki/Deadpool_(film)#/media/File:Deadpool_(2016_poster).png", TrailerUrl = "https://www.youtube.com/watch?v=ONHBaC-pfsk" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 4, Title = "The Matrix", FranchiseId = 6, Genre = "Action, Sci-Fi", ReleaseYear = 1999, Director = "Bill Pope", PictureUrl = "https://en.wikipedia.org/wiki/The_Matrix_(franchise)#/media/File:Ultimate_Matrix_Collection_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=9ix7TUGVYIo" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 5, Title = "Transformers: Dark of the Moon", FranchiseId = 4, Genre = "Action, Sci-Fi, adventure", ReleaseYear = 2014, Director = "Michael Bay", PictureUrl = "https://en.wikipedia.org/wiki/Transformers:_Dark_of_the_Moon#/media/File:Transformers_dark_of_the_moon_ver5.jpg", TrailerUrl = "https://www.youtube.com/watch?v=A9OQSaegmvc" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 6, Title = "Avengers: Infinity War", FranchiseId = 1, Genre = "Action, Sci-Fi, Comedy, Adventure", ReleaseYear = 2018, Director = "Anthony and Joe Russo", PictureUrl = "https://en.wikipedia.org/wiki/Avengers:_Infinity_War#/media/File:Avengers_Infinity_War_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=6ZfuNTqbHE8" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 7, Title = "Avengers: Endgame", FranchiseId = 1, Genre = "Action, Sci-Fi,  Comedy, Adventure, Romantic", ReleaseYear = 2019, Director = "Anthony and Joe Russo", PictureUrl = "https://en.wikipedia.org/wiki/Avengers:_Endgame#/media/File:Avengers_Endgame_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=TcMBFSGVi1c" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 8, Title = "X-Men: Days of Future Past", FranchiseId = 3, Genre = "Action, Sci-Fi", ReleaseYear = 2014, Director = "Bryan Singer", PictureUrl = "https://en.wikipedia.org/wiki/X-Men:_Days_of_Future_Past#/media/File:X-Men_Days_of_Future_Past_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=pK2zYHWDZKo" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 9, Title = "X-Men: The Last Stand", FranchiseId = 3, Genre = "Action, Sci-Fi, Comedy", ReleaseYear = 2006, Director = "Brett Ratner", PictureUrl = "https://en.wikipedia.org/wiki/X-Men:_The_Last_Stand#/media/File:X-Men_The_Last_Stand_theatrical_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=ZQ0v5dXbw7M" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 10, Title = "Pirates of the Caribbean: Dead Man's Chest", FranchiseId = 5, Genre = "Action, Sci-Fi, Comedy", ReleaseYear = 2006, Director = "Gore Verbinski", PictureUrl = "https://en.wikipedia.org/wiki/Pirates_of_the_Caribbean:_Dead_Man%27s_Chest#/media/File:Pirates_of_the_caribbean_2_poster_b.jpg", TrailerUrl = "https://www.youtube.com/watch?v=SNA-Ezahmok" });


            // Seed many-to-many movie-character. Need to define many-to-many and access linking table
            // One movie contains many characters, and a character can play in multiple movies.
            modelBuilder.Entity<Movie>()
            .HasMany(m => m.Characters)
            .WithMany(c => c.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 9, MovieId = 6 },
                            new { CharacterId = 9, MovieId = 7 },
                            new { CharacterId = 9, MovieId = 2 },
                            new { CharacterId = 1, MovieId = 10 },
                            new { CharacterId = 7, MovieId = 7 },
                            new { CharacterId = 7, MovieId = 6 },
                            new { CharacterId = 7, MovieId = 2 },
                            new { CharacterId = 4, MovieId = 3 },
                            new { CharacterId = 7, MovieId = 1 }
                        );
                    });
        }
    }
}

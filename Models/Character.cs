﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment6.Models
{


    public class Character
    {
        // PK
        public int Id { get; set; }


        // Fields
        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }


        [MaxLength(50)]
        public string? Alias { get; set; }

        [MaxLength(50)]
        public string? Gender { get; set; }

        [MaxLength(300)]
        public string? Picture { get; set; }

        // Relationships
        public virtual ICollection<Movie> Movies { get; set; }
    }
    
}


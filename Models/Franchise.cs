﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Assignment6.Models
{
    public class Franchise
    {
        // PK
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(400)]
        public string Description { get; set; }
        // Relationships
        public virtual ICollection<Movie> Movies { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment6.Models;

public class Movie
{
    // PK
    public int Id { get; set; }
    // Fields
    [MaxLength(200)] 
    public string Title { get; set; }
        
    [MaxLength(100)]
    public string Genre { get; set; }
    public int ReleaseYear { get; set; }
    [MaxLength(100)]
    public string Director { get; set; }
    [MaxLength(300)]
    public string PictureUrl { get; set; }
    [MaxLength(300)]
    public string TrailerUrl { get; set; }



    // Relationships
    public int? FranchiseId { get; set; }

    public Franchise Franchise { get; set; }

    public ICollection<Character> Characters { get; set; }
}
# ASP.NET Core WebApi with C#, Azure Data & Swagger

In this repository, We build a WebAPI with ASP.NET Core.
This WebAPI dealing with Movies, Movie Characters, Moive Franchise.
Each object have a controller which You can GET/POST/PUT/PATCH and DELETE them and display with Swagger.


## Versions
We use .Net 6.0 and Visual Studio 2020 for Mac.
This repository also use Automapper.


```https://localhost:7074/swagger```


## GET all Characters

```https://localhost:7074/api/Character ```



## GET single character (ex: 1)

``` https://localhost:7074/api/Character/1```



## POST A character

```https://localhost:7074/api/Character ```

```javascript
  {
    "fullName": "Tom Holland",
    "alias": "spider man",
    "gender": "male",
    "picture": "url ......."
  }
```



## PUT a Character by reasign value

```https://localhost:7074/api/Character?id=1 ```

``` javascript
{
  "id": 1,
  "fullName": "Leo",
  "alias": "string",
  "gender": "string",
  "picture": "string"

}
```




## DELETE a character

``` https://localhost:7074/api/Character ```

``` javascript
{
  "id": 1
}
```
> With franchise and movie we also have generic Crud.

## Extra feature

1. Relation between table:
- Characters and movies:
One movie contains many characters, and a character can play in multiple movies.

- Movies and franchises:
One movie belongs to one franchise, but a franchise can contain many movies.

2. Some others addition api endpoint:

- Updating characters in a movie.

```https://localhost:7074/api/Movie/1/Character```

- Updating movies in a franchise.

```https://localhost:7074/api/Franchise/2/Movie```

- Get all the movies in a franchise.

(https://localhost:7074/api/Franchise/1/Movies)

- Get all the characters in a movie.

(https://localhost:7074/api/Movie/1/Characters)

- Get all the characters in a franchise.

(https://localhost:7074/api/Franchise/3/Characters)

## Authors and Acknowledgements

This project was created by Linh Trieu(@linhmillion) and Nienke Kapers (@nienkek2101)

## Project Status

All features completed

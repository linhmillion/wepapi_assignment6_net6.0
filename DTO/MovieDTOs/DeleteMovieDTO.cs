namespace Assignment6.DTO.MovieDTOs;

public class DeleteMovieDTO
{
    public int Id { get; set; }
}
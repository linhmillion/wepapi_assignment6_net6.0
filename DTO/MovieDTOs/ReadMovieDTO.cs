namespace Assignment6.DTO.MovieDTOs;

public class ReadMovieDTO
{
    public int Id { get; set; }
    public string Title { get; set; }
    
    public string Genre { get; set; }
    public int ReleaseYear { get; set; }
    public string Director { get; set; }
    public string PictureUrl { get; set; }
    public string TrailerUrl { get; set; }
    
    public int FranchiseId { get; set; }
    public List<int> CharacterIds { get; set; }
}
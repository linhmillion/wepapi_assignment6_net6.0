using System.ComponentModel.DataAnnotations;

namespace Assignment6.DTO.MovieDTOs;

public class UpdateMovieDTO
{
    public int Id { get; set; }

    [MaxLength(200)] 
    public string Title { get; set; }
    [MaxLength(100)]
    public string Genre { get; set; }
    public int ReleaseYear { get; set; }
    [MaxLength(100)]
    public string Director { get; set; }
    [MaxLength(300)]
    public string PictureUrl { get; set; }
    [MaxLength(300)]
    public string TrailerUrl { get; set; }
}
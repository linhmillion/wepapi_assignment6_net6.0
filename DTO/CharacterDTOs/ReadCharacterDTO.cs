using System.ComponentModel.DataAnnotations;

namespace Assignment6.DTO.CharacterDTOs;

public class ReadCharacterDTO
{
    public int Id { get; set; }
    public string FullName { get; set; }
    public string Alias { get; set; }
    public string Gender { get; set; }
    public string Picture { get; set; }
    public List<int> MovieIds { get; set; }
}
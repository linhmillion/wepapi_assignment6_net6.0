using System.ComponentModel.DataAnnotations;

namespace Assignment6.DTO.CharacterDTOs;

public class CreateCharachterDTO
{
    [Required]
    [MaxLength(100)]
    public string FullName { get; set; }
    [MaxLength(50)]
    public string Alias { get; set; }
    [MaxLength(50)]
    public string Gender { get; set; }
    [MaxLength(300)]
    public string Picture { get; set; }
}
using System.ComponentModel.DataAnnotations;

namespace Assignment6.DTO.CharacterDTOs;

public class DeleteCharacterDTO
{
    public int Id { get; set; }
}
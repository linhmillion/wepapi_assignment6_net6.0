namespace Assignment6.DTO.FranchiseDTOs;

public class DeleteFranchiseDTO
{
    public int Id { get; set; }
}
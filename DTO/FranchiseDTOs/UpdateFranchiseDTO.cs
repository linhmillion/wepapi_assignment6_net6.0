using System.ComponentModel.DataAnnotations;

namespace Assignment6.DTO.FranchiseDTOs;

public class UpdateFranchiseDTO
{
    public int Id { get; set; }

    [Required]
    [MaxLength(200)]
    public string Name { get; set; }
    [MaxLength(400)]
    public string Description { get; set; }
}
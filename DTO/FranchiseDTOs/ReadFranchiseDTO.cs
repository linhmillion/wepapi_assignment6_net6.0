namespace Assignment6.DTO.FranchiseDTOs;

public class ReadFranchiseDTO
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    
    public List<int> MovieIds { get; set; }
}